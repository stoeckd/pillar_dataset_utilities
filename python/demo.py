#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import matplotlib

import pillar_dataset_utilities as pdu

matplotlib.rcParams['backend'] = 'TkAgg'


def main():
    # Example visualization
    # streamtrace resolution
    nx = 801
    ny = 101

    advection_9672 = np.loadtxt('../data/advection_map_9672.txt')

    # Convert to Markov matrix
    markov_9672 = pdu.advection_to_markov(advection_9672, nx, ny)

    # Create inlet for visualization
    inlet = pdu.form_inlet([0, 0, 1, 0, 0], nx, ny)

    # Create list of markov data for pillar sequence
    markov_data = []
    markov_data.append(markov_9672)

    # Create flow image
    image_9672 = pdu.flow_image([0], inlet, markov_data, nx, ny)

    # Visualization

    # Flow image
    i = plt.figure(figsize=(8, 2))
    plt.title("Flow image")
    plt.imshow(image_9672)
    i.show()

    # Quiver
    pdu.visualize_advection_quiver(advection_9672)

    # # Markov matrix
    m = plt.figure(figsize=(5, 5))
    plt.title("Markov matrix")
    plt.spy(markov_9672, markersize=0.1)
    m.show()

    print("Enter a command to close the figures.")
    raw_input()

if __name__ == '__main__':
    main()