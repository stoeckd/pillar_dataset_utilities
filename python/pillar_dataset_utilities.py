#!/usr/bin/env python

import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import scipy as sp
from scipy.sparse import csr_matrix

matplotlib.rcParams['backend'] = 'TkAgg'


def form_inlet(channels, nx, ny):
    '''
    Creates an inlet vector of fluid states
    :param channels: patter of integer-valued fluid states (typically 0 or 1)
    e.g., channels = [0, 1, 0] for a 3-fraction channel with a filled central stream
    :param nx: streamtrace resolution in width dimesion (y direction in 3D flow)
    :param ny: streamtrace resolution in height dimesion (z direction in 3D flow)
    :return:
    '''
    bins = len(channels)
    bin_width = nx/bins

    # Create first channel of inlet
    inlet = np.array([np.ones([bin_width+1])*channels[0]])

    # Concatenate subsequence sections of the inlet
    for n in range(1, bins):
        inlet = np.concatenate((inlet,[np.ones([bin_width])*channels[n]]),axis=1)

    # Tile the inlet design for the height of the inlet
    inlet = np.tile(inlet, ny)[0]

    return inlet


def advection_to_markov(adata, nx, ny):
    '''
    Converts forward-advection data to a Markov matrix
    :param adata: advection data formatted as single column: [dx; dy]
    :param nx: streamtrace resolution in width dimension (y direction in 3D flow)
    :param ny: streamtrace resolution in height dimesion (z direction in 3D flow)
    :return: Markov matrix (forward advection)
    '''
    scaled_dx = float(nx - 1.0)
    origins = range(nx*ny)
    destinations = np.zeros(nx*ny)

    for cell in origins:
        # Origin cell coordinates
        x_o = int(cell % nx)
        y_o = int(cell / nx)

        # Advection data
        dx = round(adata[cell] * scaled_dx)
        dy = round(adata[cell + nx * ny] * scaled_dx)

        # Destination coordinates
        x_d = int(x_o + dx)
        y_d = int(y_o + dy)

        # Enforce boundary condition (probably not necessary)
        if x_d < 0:
            x_d = 0
        if y_d < 0:
            y_d = 0
        if x_d > nx-1:
            x_d = nx-1
        if y_d > ny-1:
            y_d = ny-1

        destinations[cell] = int(y_d * nx + x_d)

    values = np.ones(nx*ny)
    markov = csr_matrix((values, (origins, destinations)), shape=(nx*ny, nx*ny))

    return markov


def flow_image(sequence, inlet, markov_data, nx, ny):
    '''
    :param sequence: pillar sequence as list of integers
    :param inlet: inlet vector created from form_inlet
    :param markov_data: list of markov matrices created from advection_to_markov
    :param nx: streamtrace resolution in width dimension (y direction in 3D flow)
    :param ny: streamtrace resolution in height dimesion (z direction in 3D flow)
    :return: binary sculpted flow image
    '''
    # Multiply transition matrices in mData by inlet vector to form fluid deformation image

    num_pillars = len(sequence)
    # Initialize net deformation matrix
    mNet = markov_data[sequence[0]]

    for n in range(1, num_pillars):
        mNet = mNet * markov_data[sequence[n]]
        print("Did this even fire off?")

    # Form outlet vector, shape=(1,Ny*Nz)
    outlet = inlet * mNet

    # Reshape outlet vector to nx, ny dimensions - NOT NEEDED for fitness function
    # Typically used for making an image to print
    outlet = np.rot90(outlet.reshape(nx, ny, order='F'))

    # Convert image to binary (perhaps not necessary)
    outlet = sp.sign(outlet)

    return outlet


def visualize_advection_quiver(advection_map, nx=801, ny=101):
    # Hard-coded sampling for decent visual
    sampling = 15    
    X, Y = np.meshgrid(np.arange(0, nx), np.arange(0,ny))
    X2 = np.reshape(X, (nx*ny,1))
    Y2 = np.reshape(Y,(nx*ny,1))
    q = plt.figure(figsize=(8, 2))
    plt.title("Quiver plot")
    plt.quiver(X2[::15], Y2[::15], advection_map[:nx*ny:15], advection_map[nx*ny::15])
    q.show()


def jobID_to_params(jobID):
    '''
    Converts dataset job ID to the physical parameters from the 3D flow simulation
    '''
    # Hard-coded values for large dataset
    num_d = 31
    num_y = 26
    num_re = 4
    num_h = 4
    
    max_d = 0.8
    inc_d = 0.02
    max_y = 0.5
    inc_y = 0.02    
    max_re = 40.0
    inc_re = 10.0
    
    ih = int(jobID/(num_re*num_y*num_d))
    iD = int((jobID%(num_re*num_y*num_d))/(num_re*num_y))
    iy = int(((jobID%(num_re*num_y*num_d))%(num_re*num_y))/num_re)
    iRe = int(((jobID%(num_re*num_y*num_d))%(num_re*num_y))%num_re)
        
    h = pow(0.5,ih-1.0)
    d = max_d-inc_d*iD
    y = max_y-inc_y*iy
    Re = max_re-inc_re*iRe
    
    return [h, d, y, Re]
    
if __name__ == '__main__':
    # streamtrace resolution
    nx = 801
    ny = 101

    advection_9672 = np.loadtxt('advection_map_9672.txt')

    # Convert to Markov matrix
    markov_9672 = advection_to_markov(advection_9672, nx, ny)

    # Create inlet for visualization
    inlet = form_inlet([0, 0, 1, 0, 0], nx, ny)

    # Create list of markov data for pillar sequence
    markov_data = []
    markov_data.append(markov_9672)

    image_9672 = flow_image([0], inlet, markov_data, nx, ny)


